<?php
include ('cek.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris Barang </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>
                        <ul class="nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php                              echo $_SESSION['petugas']?>
                                <b class="caret"></b></a>
                               
                            </li>
                            <li><a href="#"></a></li>
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                    <?php
                    if ($_SESSION['id_level']==1){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="inventaris.php"><i class="menu-icon icon-dashboard"></i>Inventaris
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman </a>
                                </li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-inbox"></i>Pengembalian <b class="label green pull-right">
                                    </b> </a></li>
                                <li><a href="ruang.php"><i class="menu-icon icon-tasks"></i>Ruang<b class="label orange pull-right">
                                    </b> </a></li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                            <ul class="widget widget-menu unstyled">
                                <li><a href="petugas.php"><i class="menu-icon icon-bold"></i> Petugas</a></li>
                                <li><a href="pegawai.php"><i class="menu-icon icon-bold"></i> Pegawai</a></li>
                                <li><a href="jenis.php"><i class="menu-icon icon-book"></i>Jenis </a></li>
                            </ul>
                    </div>';
                }
                        elseif ($_SESSION['id_level']==2){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-bullhorn"></i>Pengembalian</a>
                                </li>
                            </ul>
                    </div>';
                }
                       elseif ($_SESSION['id_level']==3){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                
                            </ul>
                    </div>';
                }
                ?>
                <!--/span-->
                <div class="span8" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Selamat Datang</h4>
                        	Form Pengembalian</div>
                        	
                    	</div>
                        <div class="row-fluid">
                        <!-- block -->
					
							
                        <div class="row-fluid">
                        <!-- block -->
                        
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
									<br>

                            <div class="block-content collapse in">
                                <div class="span12">
                            <div class="block-content collapse in">
                                <div class="span12">
                                    
                                <div class="module-body">
                                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                                  <thead>
                            
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Status Peminjaman</th>
                            <th>Pegawai</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
        include 'koneksi.php';
        $no = 1;
        $select = mysql_query("select * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai");
        while($data = mysql_fetch_array($select)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['tanggal_pinjam'] ?></td>
                            <td><?php echo $data['tanggal_kembali'] ?></td>
                            <td><?php echo $data['status_peminjaman'] ?></td>
                            <td><?php echo $data['nama_pegawai'] ?></td>
                            <td><a class="btn btn-primary" href="edit_pengembalian.php?id=<?php echo $data['id'] ?>">Kembalikan</a>
                            <a type="button" class="btn btn-danger fa fa-trash" href="hapus_pengembalian.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td>
                        </tr>
                        <?php 
        }
        ?>
                </table>
                <br><br>
                <h2>Tabel Data Peminjaman Detail</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
        include 'koneksi.php';
        $no = 1;
            $select = mysql_query("select peminjaman_detail.jumlah, inventaris.nama, peminjaman_detail.id_peminjaman from peminjaman_detail 
                JOIN inventaris ON peminjaman_detail.id_inventaris = inventaris.id_inventaris JOIN peminjaman ON peminjaman_detail.id_peminjaman = peminjaman.id");
        while($data = mysql_fetch_array($select)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $data['nama'] ?></td>
                            <td><?php echo $data['jumlah'] ?></td>
                            <td>
                            <center><a type="button" class="btn btn-danger fa fa-trash" href="hapus_peminjaman_detail.php?id=<?php echo $data['id'] ?>"><b>X</b></a></td></center>
                        </tr>
                        <?php 
        }
        ?>
                </table>
                <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example').DataTable();
                                        });
                                        </script>
                                                        <script type ="text/javascript" src="assets/js/jquery.min.js"></script>
                                        <script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                        <script>$(document).ready(function(){
                                            $('#example2').DataTable();
                                        });
                                        </script>

            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

                     <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
           
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
          <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>
</body>

</html>