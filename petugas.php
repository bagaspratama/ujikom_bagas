<?php
include ('cek.php');
?>
<?php
include ('cek_level.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
        rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris Barang </a>
                        <div class="nav-collapse collapse navbar-inverse-collapse">
                            <ul class="nav nav-icons">
                                <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                                <li><a href="#"><i class="icon-eye-open"></i></a></li>
                                <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                            </ul>
                            <form class="navbar-search pull-left input-append" action="#">
                                <input type="text" class="span3">
                                <button class="btn" type="button">
                                    <i class="icon-search"></i>
                                </button>
                            </form>
                            <ul class="nav pull-right">
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php 
                                   echo $_SESSION['petugas']?>
                                   <b class="caret"></b></a>

                               </li>
                               <li><a href="#"> </a></li>
                               <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <?php
                if ($_SESSION['id_level']==1){
                   echo'<div class="span3" id="sidebar">
                   <ul class="widget widget-menu unstyled">
                    <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                    </a></li>
                    <li class="active"><a href="inventaris.php"><i class="menu-icon icon-dashboard"></i>Inventaris
                    </a></li>
                    <li><a href="peminjaman.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman </a>
                    </li>
                    <li><a href="pengembalian.php"><i class="menu-icon icon-inbox"></i>Pengembalian <b class="label green pull-right">
                    </b> </a></li>
                    <li><a href="ruang.php"><i class="menu-icon icon-tasks"></i>Ruang<b class="label orange pull-right">
                    </b> </a></li>
                </ul>
                <!--/.widget-nav-->


                <ul class="widget widget-menu unstyled">
                    <li><a href="petugas.php"><i class="menu-icon icon-bold"></i> Petugas</a></li>
                    <li><a href="pegawai.php"><i class="menu-icon icon-bold"></i> Pegawai</a></li>
                    <li><a href="jenis.php"><i class="menu-icon icon-book"></i>Jenis </a></li>
                </ul>
            </div>';
        }
        elseif ($_SESSION['id_level']==2){
           echo'<div class="span3" id="sidebar">
           <ul class="widget widget-menu unstyled">
            <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
            </a></li>
            <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
            </a></li>
            <li><a href="pengembalian.php"><i class="menu-icon icon-bullhorn"></i>Pengembalian</a>
            </li>
        </ul>
    </div>';
}
elseif ($_SESSION['id_level']==3){
   echo'<div class="span3" id="sidebar">
   <ul class="widget widget-menu unstyled">
    <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
    </a></li>
    <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
    </a></li>

</ul>
</div>';
}
?>

<div class="span8">
    <div class="content">

        <div class="module">
            <div class="module-head">
                <h3>Forms</h3>
            </div>

            <!-- block -->
            <br>    
            <div class="block-content collapse in">
                <div class="content">
                  <form action="simpan_petugas.php" method="post" class="form-horizontal">
                      <fieldset>

                         <div class="control-group">
                          <label class="control-label" for="typeahead">Username</label>
                          <div class="controls">
                            <input name="username" type="text"  placeholder="" class="span6" id="typeahead" required="" data-provide="typeahead" 
                            data-items="4" >
                        </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" for="typeahead">Password</label>
                      <div class="controls">
                        <input name="password" type="password" md5  id="typeahead" required="" data-provide="typeahead"
                        data-items="4">
                        <input type="checkbox" id="showPass">Show Password
                    </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="typeahead">Nama petugas</label>
                  <div class="controls">
                    <input name="nama_petugas" type="text"  placeholder="" class="span6" id="typeahead" required=""  data-provide="typeahead" 
                    data-items="4" >
                </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="typeahead">Id level </label>
              <div class="controls">
                  <select name="id_pegawai" class="form-control"> 
                    <?php
                    include "koneksi.php";
                    $select = mysql_query("SELECT * FROM level");
                    while($data = mysql_fetch_array($select))
                    {
                        ?>
                        <center>
                          <option value="<?php echo $data['id_level'];?>"><?php echo $data['nama_level'];?></option>
                      </center>
                      <?php } ?>
                  </select>
              </div>
          </div>
          <br>
          <p align="center">
            <button type="submit" class="btn btn-success">Simpan</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </p>
    </fieldset>
</form>
</div>
</div>
</div>

<div class="content">

  <div class="module">
     <div class="module-head">
        <h3>Tables</h3>
    </div>
    <div class="module-body">


        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
          <thead>
           <tr>
             <th>Id petugas</th>
             <th>Username</th>
             <th>Password</th>
             <th>Nama</th>
             <th>Level</th>
             <th>Aksi</th>
         </tr>
     </thead>
     <tbody>

        <?php
        include "koneksi.php";
        $no=1;
        $select=mysql_query("select * from petugas");
        while($data=mysql_fetch_array($select))
        {
            ?>
            <tr>
                <td><?php echo $no++; ?></td>

                <td><?php echo $data['username']; ?></td>
                <td><?php echo $data['password']; ?></td>
                <td><?php echo $data['nama_petugas']; ?></td>
                <td><?php echo $data['id_level']; ?></td>


                <td>
                    <a class="btn btn outline btn-primary icon-edit" href="edit_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>"></a> 
                    <a class="btn btn outline btn-danger icon-trash" href="hapus_petugas.php?id_petugas=<?php echo $data['id_petugas']; ?>"></a>

                </td>
            </tr>
            <?php
        }
        ?>

    </tbody>								

</table></div>
<br>
<a href="tambah_barang.php" class="btn btn-success fa fa-plus">Export Data</a>

<div class="footer">
    <div class="container">
        <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
    </div>
</div>
<script src="scripts/jquery-1.9.1.min.js"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    } );
</script>
<script>
    $(function(){
        $("#showPass").click(function(){
            if($("[name=password]").attr('type')=='password'){
                $("[name=password]").attr('type','text');
        }else{
            $("[name=password]").attr('type','password');
        }
    });
});

    </script>
</body>