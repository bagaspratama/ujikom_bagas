<?php
include ('cek.php');
?>
<?php
include ('cek_level.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Edmin</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Inventaris Barang </a>
                    
                        <form class="navbar-search pull-left input-append" action="#">
                        <input type="text" class="span3">
                        <button class="btn" type="button">
                            <i class="icon-search"></i>
                        </button>
                        </form>
                        <ul class="nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php 
                             echo $_SESSION['petugas']?>
                                <b class="caret"></b></a>
                               
                            </li>
                        
                              <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
       <div class="wrapper">
            <div class="container">
                    <?php
                    if ($_SESSION['id_level']==1){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="inventaris.php"><i class="menu-icon icon-dashboard"></i>Inventaris
                                </a></li>
                                <li><a href="peminjaman.php"><i class="menu-icon icon-bullhorn"></i>Peminjaman </a>
                                </li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-inbox"></i>Pengembalian <b class="label green pull-right">
                                    </b> </a></li>
                                <li><a href="ruang.php"><i class="menu-icon icon-tasks"></i>Ruang<b class="label orange pull-right">
                                    </b> </a></li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                            <ul class="widget widget-menu unstyled">
                                <li><a href="pegawai.php"><i class="menu-icon icon-bold"></i> Pegawai</a></li>
                                <li><a href="jenis.php"><i class="menu-icon icon-book"></i>Jenis </a></li>
                            </ul>
                    </div>';
                }
                        elseif ($_SESSION['id_level']==2){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                <li><a href="pengembalian.php"><i class="menu-icon icon-bullhorn"></i>Pengembalian</a>
                                </li>
                            </ul>
                    </div>';
                }
                       elseif ($_SESSION['id_level']==3){
                         echo'<div class="span3" id="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li class="active"><a href="peminjaman.php"><i class="menu-icon icon-dashboard"></i>Peminjaman
                                </a></li>
                                
                            </ul>
                    </div>';
                }
                ?>
        
                        
           <div class="span8">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Forms</h3>
							</div>
				
                        <!-- block -->
                       	<br>	
                            <div class="block-content collapse in">
                                <div class="content">
                                  <form action="simpan_inventaris.php" method="post" class="form-horizontal">
                                      <fieldset>
                                       
                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Nama </label>
                                          <div class="controls">
                                            <input name="nama" type="text"  placeholder="Masukan Nama Inventaris" class="span6" id="typeahead" required="" data-provide="typeahead" 
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kondisi </label>
                                          <div class="controls">
                                            <select option name="kondisi" type="text" placeholder="Masukan Tanggal Kembali" class="span6" id="typeahead" required="" data-provide="typeahead"
											data-items="4" >
                      <option>Tidak baik</optio>
                      <option>Baik</option>
                      </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Keterangan </label>
                                          <div class="controls">
                                            <input name="keterangan" type="text"  placeholder="Masukan Status Peminjaman" class="span6" id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                          <div class="controls">
                                            <input name="jumlah" type="number"  id="typeahead" required="" data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>

                                       <div class="control-group">
                                          <label class="control-label" for="typeahead">Jenis </label>
                                          <div class="controls">
                                          <select name="id_jenis" class="form-control"> 
                    <?php
                                             include "koneksi.php";
                  $select = mysql_query("SELECT * FROM jenis");
                  while($data = mysql_fetch_array($select))
                  {
                    ?>
                    <center>
                      <option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
                    </center>
                    <?php } ?>
			 </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Tanggal Register </label>
                                          <div class="controls">
                                            <input name="tanggal_register" type="date"  id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Ruang </label>
                                          <div class="controls">
                                          <select name="id_ruang"  class="span6" class="form-control"> 
                    <?php
                                             include "koneksi.php";
                  $select = mysql_query("SELECT * FROM ruang");
                  while($data = mysql_fetch_array($select))
                  {
                    ?>
                    <center>
                      <option value='<?php echo $data['id_ruang'];?>'><?php echo $data['nama_ruang'];?></option>
                    </center>
                    <?php } ?>
			 </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Kode Inventaris</label>
                                          <div class="controls">
                                            <input name="kode_inventaris" type="text" placeholder="Masukan Kode Inventaris" class="span6" id="typeahead"  data-provide="typeahead"
											data-items="4" >
                                            </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="typeahead">Petugas </label>
                                          <div class="controls">
                                          <select name="id_petugas"  class="span6" class="form-control"> 
                    <?php
                                             include "koneksi.php";
                  $select = mysql_query("SELECT * FROM petugas");
                  while($data = mysql_fetch_array($select))
                  {
                    ?>
                    <center>
                      <option value='<?php echo $data['id_petugas'];?>'><?php echo $data['nama_petugas'];?></option>
                    </center>
                    <?php } ?>
			 </select>
                                            </div>
                                        </div>
                                        <br>
                              
                                      
										<button type="submit" class="btn btn-success">Simpan</button>
										<button type="reset" class="btn btn-danger">Reset</button>
                                 
                                      </fieldset>
                                    </form>
                                </div>
							</div>
                            </div>

						
			       <!--/.wrapper-->
                   <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="scripts/common.js" type="text/javascript"></script>
      
    </body>
